#ifndef __NAVECITA_EVENT_RECEIVER_HPP_INCLUDED__
#define __NAVECITA_EVENT_RECEIVER_HPP_INCLUDED__

#include <irrlicht/irrlicht.h>

using namespace irr;

class EventReceiver : public IEventReceiver
{
public:
    // This is the one method that we have to implement
    virtual bool OnEvent(const SEvent& event);

    // This is used to check whether a key is being held down
    virtual bool IsKeyDown(EKEY_CODE keyCode) const;

    EventReceiver();
private:
    // We use this array to store the current state of each key
    bool KeyIsDown[KEY_KEY_CODES_COUNT];
};
#endif // ifndef __NAVECITA_EVENT_RECEIVER_HPP_INCLUDED__
