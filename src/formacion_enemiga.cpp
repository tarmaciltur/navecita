#include "formacion_enemiga.hpp"
#include "nave_enemiga.hpp"
#include <irrlicht/irrlicht.h>

using namespace irr;

const f32 FormacionEnemiga::MOVEMENT_SPEED = 5.f;

FormacionEnemiga::FormacionEnemiga (scene::ISceneNode * node)
{
    this->node = node;
    this->tiempo_direccion = 0;
    this->direccion        = 1;

    this->naves = new core::list<NaveEnemiga *> ();
}

void FormacionEnemiga::mover(float delta)
{
    core::vector3df position = this->node->getPosition();

    if (this->tiempo_direccion < 12)
    {
        this->tiempo_direccion += delta;
    }
    else
    {
        this->tiempo_direccion = 0;
        this->direccion       *= -1;
    }

    position.Z += delta * FormacionEnemiga::MOVEMENT_SPEED * this->direccion;
    this->node->setPosition(position);
}

void FormacionEnemiga::agregarNave(NaveEnemiga * nave)
{
    scene::ISceneNode * nave_node = nave->getNode();

    nave_node->setParent(this->node);

    core::vector3df position = nave_node->getPosition();
    position.Z += ( this->naves->size() % 10 ) * 8;
    position.X -= 8 * this->naves->size() / 10;
    nave_node->setPosition(position);

    this->naves->push_back(nave);
}
