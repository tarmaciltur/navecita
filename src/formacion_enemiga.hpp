#ifndef __NAVECITA_FORMACION_ENEMIGA_HPP_INCLUDED__
#define __NAVECITA_FORMACION_ENEMIGA_HPP_INCLUDED__

#include <irrlicht/irrlicht.h>
#include "nave_enemiga.hpp"

using namespace irr;

class FormacionEnemiga {
public:
    static const f32 MOVEMENT_SPEED;
    float tiempo_direccion;
    int direccion;

    FormacionEnemiga (scene::ISceneNode * node);

    void mover(float delta);
    void agregarNave(NaveEnemiga * nave);
private:
    scene::ISceneNode * node;
    core::list<NaveEnemiga *> * naves;
};
#endif // ifndef __NAVECITA_FORMACION_ENEMIGA_HPP_INCLUDED__
