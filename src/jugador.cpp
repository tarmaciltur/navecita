#include "jugador.hpp"
#include <irrlicht/irrlicht.h>

using namespace irr;

const f32 Jugador::MOVEMENT_SPEED = 10.f;

Jugador::Jugador (scene::ISceneNode * node, scene::ICameraSceneNode * camera)
{
    this->node   = node;
    this->camera = camera;

    this->camera->setParent(this->node);
    this->camera->setPosition(core::vector3df(5, 0, 2.82));
    this->update_camera_target();
}

void Jugador::mover(float delta)
{
    core::vector3df position = this->node->getPosition();

    position.Z += delta * Jugador::MOVEMENT_SPEED;
    this->node->setPosition(position);
    this->update_camera_target();
}

void Jugador::update_camera_target()
{
    core::vector3df jugadorPosition = this->node->getPosition();

    jugadorPosition.Z += 2.82;
    this->camera->setTarget(jugadorPosition);
}
