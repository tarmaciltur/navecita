#ifndef __NAVECITA_JUGADOR_HPP_INCLUDED__
#define __NAVECITA_JUGADOR_HPP_INCLUDED__

#include <irrlicht/irrlicht.h>

using namespace irr;

class Jugador {
public:
    static const f32 MOVEMENT_SPEED;

    Jugador (scene::ISceneNode * node, scene::ICameraSceneNode * camera);

    void mover(float delta);
private:
    scene::ISceneNode * node;
    scene::ICameraSceneNode * camera;

    void update_camera_target();
};
#endif // ifndef __NAVECITA_JUGADOR_HPP_INCLUDED__
