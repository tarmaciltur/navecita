#include <irrlicht/irrlicht.h>
#include <string>
#include <iostream>

#include "config.h"

#include "event_receiver.hpp"
#include "jugador.hpp"
#include "formacion_enemiga.hpp"
#include "nave_enemiga.hpp"

using namespace irr;
using namespace scene;
using namespace video;
using namespace gui;

int main(int argc, char ** argv)
{
    video::E_DRIVER_TYPE driverType = video::EDT_OPENGL;

    EventReceiver receiver;

    IrrlichtDevice * device = createDevice(driverType,
        core::dimension2d<u32>(1280, 720), 16, false, false, false, &receiver);

    if (device == 0)
    {
        return 1; // could not create selected driver.
    }

    std::string install_dir = PREFIX;

    device->setWindowCaption(L"Navecita Loca");

    IVideoDriver * driver    = device->getVideoDriver();
    ISceneManager * smgr     = device->getSceneManager();
    IGUIEnvironment * guienv = device->getGUIEnvironment();

    IAnimatedMesh * mesh_nave = smgr->getMesh(install_dir.append("/share/navecita/models/nave.obj").data());


    scene::IAnimatedMeshSceneNode * player_node =
      smgr->addAnimatedMeshSceneNode(mesh_nave);

    player_node->setMaterialFlag(video::EMF_LIGHTING, false);

    scene::ICameraSceneNode * camera = smgr->addCameraSceneNode();
    device->getCursorControl()->setVisible(false);

    Jugador * jugador = new Jugador(player_node, camera);


    scene::ISceneNode * enemigos_node = smgr->addEmptySceneNode();
    enemigos_node->setPosition(core::vector3df(-50, 0, -50));

    FormacionEnemiga * enemigos = new FormacionEnemiga(enemigos_node);

    scene::IAnimatedMeshSceneNode * ref_node;
    NaveEnemiga * enemigo;
    for (int i = 0; i < 30; i++)
    {
        ref_node = smgr->addAnimatedMeshSceneNode(mesh_nave);

        ref_node->setMaterialFlag(video::EMF_LIGHTING, false);

        enemigo = new NaveEnemiga(ref_node);

        enemigos->agregarNave(enemigo);
    }


    int lastFPS = -1;

    // In order to do framerate independent movement, we have to know
    // how long it was since the last frame
    u32 then = device->getTimer()->getTime();


    while (device->run())
    {
        // Work out a frame delta time.
        const u32 now = device->getTimer()->getTime();
        const f32 frameDeltaTime = (f32) (now - then) / 1000.f; // Time in seconds
        then = now;

        int direccion = 0;
        if (receiver.IsKeyDown(irr::KEY_KEY_A))
        {
            direccion = -1;
        }
        else
        {
            if (receiver.IsKeyDown(irr::KEY_KEY_D))
            {
                direccion = 1;
            }
        }

        if (direccion != 0)
        {
            jugador->mover(direccion * frameDeltaTime);
        }

        enemigos->mover(frameDeltaTime);

        driver->beginScene(true, true, video::SColor(255, 0, 0, 0));

        smgr->drawAll();                        // draw the 3d scene
        device->getGUIEnvironment()->drawAll(); // draw the gui environment (the logo)

        driver->endScene();

        int fps = driver->getFPS();

        if (lastFPS != fps)
        {
            core::stringw tmp(L"Navecita - Irrlicht Engine [");
            tmp += driver->getName();
            tmp += L"] fps: ";
            tmp += fps;

            device->setWindowCaption(tmp.c_str());
            lastFPS = fps;
        }
    }
    device->drop();

    return 0;
} // main
