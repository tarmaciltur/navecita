#include "nave_enemiga.hpp"
#include <irrlicht/irrlicht.h>

using namespace irr;

NaveEnemiga::NaveEnemiga (scene::ISceneNode * node)
{
    this->node = node;
}

void NaveEnemiga::mover(float delta)
{
    core::vector3df position = this->node->getPosition();

    position.Z += delta;
    position.X += delta;
    position.Y += delta;
    this->node->setPosition(position);
}

bool NaveEnemiga::getAtacando()
{
    return this->atacando;
}

void NaveEnemiga::setAtacando(bool atacando)
{
    this->atacando = atacando;
}

scene::ISceneNode * NaveEnemiga::getNode()
{
    return this->node;
}
