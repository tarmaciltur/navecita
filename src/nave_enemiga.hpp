#ifndef __NAVECITA_NAVE_ENEMIGA_HPP_INCLUDED__
#define __NAVECITA_NAVE_ENEMIGA_HPP_INCLUDED__

#include <irrlicht/irrlicht.h>

using namespace irr;

class NaveEnemiga {
public:
    NaveEnemiga (scene::ISceneNode * node);

    void mover(float delta);
    bool getAtacando();
    void setAtacando(bool atacando);
    scene::ISceneNode * getNode();
private:
    scene::ISceneNode * node;
    bool atacando;
};
#endif // ifndef __NAVECITA_NAVE_ENEMIGA_HPP_INCLUDED__
